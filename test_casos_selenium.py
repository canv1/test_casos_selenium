import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class test_casos_selenium(unittest.TestCase):

    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)#Espera implícita
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.quit()

    def test_single_input_field(self):
        ''' Página número 1 : Single Input Field '''
        driver = self.driver
        driver.get('https://www.seleniumeasy.com/test/basic-first-form-demo.html')
        Mensaje='Mensaje'
        #Input message
        input_message = driver.find_element_by_xpath('/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[2]/form[1]/div[1]/input[1]')
        input_message.send_keys(Mensaje)
        #Button
        button_show = driver.find_element_by_xpath('/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[2]/form[1]/button[1]')
        button_show.click()
        #Texto mostrado en span
        span_textout = driver.find_element_by_id('display')
        self.assertEqual(Mensaje, span_textout.text)

    def test_single_checkbox_demo(self):
        ''' Single Checkbox Demo '''
        driver = self.driver
        driver.get('https://www.seleniumeasy.com/test/basic-checkbox-demo.html')
        #Checkbutton
        xpath_option_label="//label[text()='Click on this check box']"
        check_button = driver.find_element_by_xpath(xpath_option_label)
        check_button.click()
        #Muestra el mensaje de clicked :Success
        div_message = driver.find_element_by_id('txtAge')
        self.assertRegex(div_message.text, 'Success ')

    def test_radio_button_demo(self):
        ''' Radio Button Demo '''
        driver = self.driver
        driver.get('https://www.seleniumeasy.com/test/basic-radiobutton-demo.html')
        #clicking Radio button female
        radio_button_female = driver.find_element_by_xpath("//body/div[@id='easycont']/div[1]/div[2]/div[1]/div[2]/label[2]/input[1]")
        radio_button_female.click()
        #Button to check value
        button_get_value = driver.find_element_by_xpath("//button[@id='buttoncheck']")
        button_get_value.click()
        #Getting text generated
        p_radiobutton_message = driver.find_element_by_xpath("/html[1]/body[1]/div[2]/div[1]/div[2]/div[1]/div[2]/p[3]")
        self.assertRegex(p_radiobutton_message.text,'Female')

if __name__ == '__main__':
    unittest.main()
